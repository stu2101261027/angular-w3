import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,
    FormsModule,
    CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {

  tempNoteTitle = '';
  tempNoteText = '';
  modelTitle = '';
  modelText = '';
  buttonsVisible = false;
  notes = new Map<string, string>();
  public processNoteTitle(input) {
    this.tempNoteTitle = input.target.value;
  }
  public processNoteText(input) {
    this.tempNoteText = input.target.value;
  }
  public saveNote() {
    this.notes.set(this.modelTitle, this.modelText);
    this.notes.forEach((value: string, key: string) => {
      console.log(key, value);
    });
    this.clearData();
  }
  selectedNote;
  public selectNotes(elem) {
    this.buttonsVisible = true;
    this.selectedNote = elem;
  }
  public selectNote() {
    this.tempNoteTitle = this.selectedNote.key;
    this.tempNoteText = this.selectedNote.value;
    this.modelTitle = this.tempNoteTitle;
  }
  public removeSelection() {
    this.buttonsVisible = false;
  }
  public deleteNote() {
    this.notes.delete(this.selectedNote.key);
    this.clearData();
  }
  private clearData() {
    this.tempNoteText = '';
    this.tempNoteTitle = '';
    this.modelText = '';
    this.buttonsVisible = false;
  }
}
